if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    # extract CUDA version from package name so
    # we know what the executable is called
    CUDA_VER=${PKG_NAME/fsl-ptx2-cuda-/}
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper probtrackx2_gpu${CUDA_VER}
fi
