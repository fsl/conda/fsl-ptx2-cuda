if [ -e ${FSLDIR}/share/fsl/sbin/removeFSLWrapper ]; then
    CUDA_VER=${PKG_NAME/fsl-ptx2-cuda-/}
    ${FSLDIR}/share/fsl/sbin/removeFSLWrapper probtrackx2_gpu${CUDA_VER}
fi
